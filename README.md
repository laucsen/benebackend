# Weather 101

> Weather for your capital ciry!

### Requirements

1. NodeJs
2. Npm
3. Docker (To run a local database.) or a running PostgreSQL database.
 
**Note**: Running without a configured database will use sqlite.

### Installation

1. Navigate to Backend folder.
2. Run `npm install`
3. Run `npm start` to start develpment server.

**Note**: Development mode will usar port `8080`, prodution will use `80`.

### Running

Steps to run connected to a PostgreSQL database.

1. Run Postgres on your local machine. Before run your server, add environment variables as listed below.
2. Run your server with production variable to connect to database: `npm run production`

#### Environment Variables

This variables are in need to connect to PostgreSQL. Add them with your values.

```
DB_NAME=postgres
DB_USER=postgres
DB_PASS=
DB_HOST=192.168.99.100:5432
JWT_SECRET=@#$#bajisidjs@
```

### What sould be better

1. I choose this bolierplate to make things faster, but a Boilerplate with ES6 could be better. 
