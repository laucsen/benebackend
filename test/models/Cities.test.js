const {
  beforeAction,
  afterAction,
} = require('../setup/_setup');
const User = require('../../api/models/User');
const Cities = require('../../api/models/Cities');

let user;

beforeAll(async () => {
  await beforeAction();
});

afterAll(() => {
  afterAction();
});

beforeEach(async () => {
  user = await User.build({
    email: 'martin@mail.com',
    password: 'securepassword',
  }).save();
});

test('City is created correctly and related', async () => {
  const sendUser = user.toJSON();
  const c1 = await Cities.build({
    name: 'Campos Novos',
  }).save();

  c1.setUser(user);
  await c1.save();

  expect(c1.owner).toBe(sendUser.id);

  await user.destroy();
  await c1.destroy();
});
