const request = require('supertest');
const {
  beforeAction,
  afterAction,
} = require('../setup/_setup');

const User = require('../../api/models/User');
const Cities = require('../../api/models/Cities');

const mockedWeather = {
  name: 'Florianopolis',
  dt: 31312312312,
  weather: [{ icon: '10', main: '::A', description: 'nuveado' }],
  sys: {
    sunrise: 101021,
    sunset: 101021,
  },
  main: { temp: 299 },
};

jest.mock('../../api/services/getUrl', () => () => new Promise((resolve) => resolve(mockedWeather)));

let api;

beforeAll(async () => {
  api = await beforeAction();
});

afterAll(() => {
  afterAction();
});

let us;
let ct;

beforeEach(async () => {
  us = await User.build({
    email: 'george@mail.com',
    password: 'securepassword',
  }).save();
  ct = await Cities.build({
    name: 'Campos Novos',
  }).save();
  ct.setUser(us);
  await ct.save();
});

afterEach(async () => {
  await us.destroy();
  await ct.destroy();
});

test('City | Created User with Country we will try to get its Capital', async () => {
  await request(api)
    .post('/public/user')
    .set('Accept', /json/)
    .send({
      email: 'martin@mail.com',
      password: 'securepassword',
      password2: 'securepassword',
      country: 'Brazil',
    })
    .expect(200);

  const res2 = await request(api)
    .post('/public/login')
    .set('Accept', /json/)
    .send({
      email: 'martin@mail.com',
      password: 'securepassword',
    })
    .expect(200);

  expect(res2.body.token).toBeTruthy();

  const res3 = await request(api)
    .get('/private/cities/my')
    .set('Accept', /json/)
    .set('Authorization', `Bearer ${res2.body.token}`)
    .set('Content-Type', 'application/json')
    .expect(200);

  expect(res3.body.cities).toHaveLength(1);
  expect(res3.body.cities[0].name).toBe('Brasília');
});

test('City | Created User with Country, but not right', async () => {
  await request(api)
    .post('/public/user')
    .set('Accept', /json/)
    .send({
      email: 'martin2@mail.com',
      password: 'securepassword',
      password2: 'securepassword',
      country: 'Brazilll',
    })
    .expect(200);

  const res2 = await request(api)
    .post('/public/login')
    .set('Accept', /json/)
    .send({
      email: 'martin2@mail.com',
      password: 'securepassword',
    })
    .expect(200);

  expect(res2.body.token).toBeTruthy();

  const res3 = await request(api)
    .get('/private/cities/my')
    .set('Accept', /json/)
    .set('Authorization', `Bearer ${res2.body.token}`)
    .set('Content-Type', 'application/json')
    .expect(200);

  expect(res3.body.cities).toHaveLength(0);
});

test('City | Create City on logged user', async () => {
  await request(api)
    .post('/public/user')
    .set('Accept', /json/)
    .send({
      email: 'martin3@mail.com',
      password: 'securepassword',
      password2: 'securepassword',
      country: 'Brazil',
    })
    .expect(200);

  const res2 = await request(api)
    .post('/public/login')
    .set('Accept', /json/)
    .send({
      email: 'martin3@mail.com',
      password: 'securepassword',
    })
    .expect(200);

  expect(res2.body.token).toBeTruthy();

  await request(api)
    .post('/private/cities/')
    .set('Accept', /json/)
    .set('Authorization', `Bearer ${res2.body.token}`)
    .set('Content-Type', 'application/json')
    .send({
      name: 'Florianópolis',
    })
    .expect(200);

  const res4 = await request(api)
    .get('/private/cities/my')
    .set('Accept', /json/)
    .set('Authorization', `Bearer ${res2.body.token}`)
    .set('Content-Type', 'application/json')
    .expect(200);

  expect(res4.body.cities).toHaveLength(2);

  expect(res4.body.cities[0].name).toBe('Brasília');
});

test('City | Get City Weather', async () => {
  await request(api)
    .post('/public/user')
    .set('Accept', /json/)
    .send({
      email: 'mario@mail.com',
      password: 'securepassword',
      password2: 'securepassword',
      country: 'Brazil',
    })
    .expect(200);

  const res2 = await request(api)
    .post('/public/login')
    .set('Accept', /json/)
    .send({
      email: 'mario@mail.com',
      password: 'securepassword',
    })
    .expect(200);

  expect(res2.body.token).toBeTruthy();

  const res3 = await request(api)
    .get('/private/cities/info/Florianópolis')
    .set('Accept', /json/)
    .set('Authorization', `Bearer ${res2.body.token}`)
    .set('Content-Type', 'application/json')
    .expect(200);

  expect(res3.body.name).toBe('Florianopolis');
  expect(res3.body.weather).toBeTruthy();

  expect(res3.body.sunrise).toBeTruthy();
  expect(res3.body.sunset).toBeTruthy();

  expect(res3.body.temp).toBeTruthy();
});

test('City | Get City Weather | Lookup', async () => {
  await request(api)
    .post('/public/user')
    .set('Accept', /json/)
    .send({
      email: 'mario666@mail.com',
      password: 'securepassword',
      password2: 'securepassword',
      country: 'Brazil',
    })
    .expect(200);

  const res2 = await request(api)
    .post('/public/login')
    .set('Accept', /json/)
    .send({
      email: 'mario666@mail.com',
      password: 'securepassword',
    })
    .expect(200);

  expect(res2.body.token).toBeTruthy();

  const res3 = await request(api)
    .get('/private/cities/info/Brasília')
    .set('Accept', /json/)
    .set('Authorization', `Bearer ${res2.body.token}`)
    .set('Content-Type', 'application/json')
    .expect(200);

  expect(res3.body.timezone).toBe('America/Sao_Paulo');
});
