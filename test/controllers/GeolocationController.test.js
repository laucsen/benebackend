const request = require('supertest');
const {
  beforeAction,
  afterAction,
} = require('../setup/_setup');

let api;

beforeAll(async () => {
  api = await beforeAction();
});

afterAll(() => {
  afterAction();
});

test('Country | autocomplete | full', async () => {
  const res = await request(api)
    .get('/public/geolocation/contries')
    .expect(200);

  expect(res.body.length).toBe(243);
  expect(res.body[100]).toBe('India');
});

test('Country | autocomplete | filter', async () => {
  const res = await request(api)
    .get('/public/geolocation/contries?filter=bra')
    .expect(200);

  expect(res.body.length).toBe(2);
  expect(res.body[0]).toBe('Brazil');
});

test('Country | get-capital', async () => {
  const res = await request(api)
    .get('/public/geolocation/capital/Brazil')
    .expect(200);

  expect(res.body).toBe('Brasília');
});

test('Country | get-capital | wrong', async () => {
  const res = await request(api)
    .get('/public/geolocation/capital/Brazilão')
    .expect(200);

  expect(res.body).toBe('');
});

test('Cities | autocomplete | filter', async () => {
  const res = await request(api)
    .get('/public/geolocation/cities?filter=floria')
    .expect(200);

  expect(res.body.length).toBe(9);
  expect(res.body[5]).toBe('Florianópolis');
});

test('Capitals | autocomplete | full', async () => {
  const res = await request(api)
    .get('/public/geolocation/capitals')
    .expect(200);

  expect(res.body.length).toBe(200);
  expect(res.body[100]).toBe('Bishkek');
});

test('Capitals | autocomplete | filter', async () => {
  const res = await request(api)
    .get('/public/geolocation/capitals?filter=Bra')
    .expect(200);

  expect(res.body.length).toBe(3);
  expect(res.body[0]).toBe('Brasília');
});
