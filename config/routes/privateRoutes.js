const privateRoutes = {
  'GET /users': 'UserController.getAll',
  'GET /cities/my': 'CityController.getMine',
  'POST /cities/': 'CityController.create',
  'GET /cities/info/:city': 'CityController.info',
};

module.exports = privateRoutes;
