const publicRoutes = {
  'POST /user': 'UserController.register',
  'POST /register': 'UserController.register', // alias for POST /user
  'POST /login': 'UserController.login',
  'POST /validate': 'UserController.validate',
  'GET /geolocation/contries': 'GeolocationController.countries',
  'GET /geolocation/cities': 'GeolocationController.city',
  'GET /geolocation/capital/:country': 'GeolocationController.capital',
  'GET /geolocation/capitals': 'GeolocationController.capitals',
};

module.exports = publicRoutes;
