const http = require('http');

const getUrl = (url) =>
  new Promise((resolve, reject) => {
    http.get(url, (res) => {
      const { statusCode } = res;
      const contentType = res.headers['content-type'];

      let error;
      if (statusCode !== 200) {
        error = {
          status: statusCode,
          message: 'Request Failed.',
        };
      } else if (!/^application\/json/.test(contentType)) {
        error = {
          status: statusCode,
          message: 'Invalid content-type.\n' +
            `Expected application/json but received ${contentType}`,
        };
      }
      if (error) {
        // consume response data to free up memory
        reject(error);
        res.resume();
        return;
      }

      res.setEncoding('utf8');
      let rawData = '';
      res.on('data', (chunk) => { rawData += chunk; });
      res.on('end', () => {
        try {
          const parsedData = JSON.parse(rawData);
          resolve(parsedData);
        } catch (e) {
          reject(e);
        }
      });
    }).on('error', (e) => {
      reject(e);
    });
  });

module.exports = getUrl;
