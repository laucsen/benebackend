const removeDiacritics = require('diacritics').remove;

const getUrl = require('../services/getUrl');
const config = require('../../config');
const cityTimezones = require('city-timezones');

const Cities = require('../models/Cities');

const WEATHER_URL = 'http://api.openweathermap.org/data/2.5/weather';

const CityController = () => {
  const getMine = async (req, res) => {
    try {
      const cities = await Cities.findAll({
        where: {
          owner: req.token.id,
        },
      });

      const ordered = cities.sort((a, b) => {
        if (a === b) {
          return 0;
        }
        if (a === true) {
          return 1;
        }
        return -1;
      });
      return res.status(200).json({ cities: ordered });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const create = async (req, res) => {
    try {
      const { name } = req.body;
      if (!name) {
        return res.status(400).json({ msg: 'Missing params to create a city' });
      }
      const city = await Cities.create({
        name: req.body.name,
      });
      city.setUser(req.token.id);
      await city.save();
      return res.status(200).json({ city });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const info = (req, res) => {
    try {
      const { city } = req.params;
      return getUrl(`${WEATHER_URL}?q=${removeDiacritics(city)}&appid=${config.weather}`)
        .then((result) => {
          const partial = {
            name: result.name,
            weather: (result.weather || [{}])[0].description,
            sunrise: (result.sys || {}).sunrise,
            sunset: (result.sys || {}).sunset,
            temp: (result.main || {}).temp,
          };

          const cityLookup = cityTimezones.lookupViaCity(removeDiacritics(city));
          if (cityLookup.length > 0) {
            return {
              ...partial,
              timezone: cityLookup[0].timezone,
            };
          }
          return partial;
        })
        .then((result) => res.status(200).json(result))
        .catch((err) => res.status(err.status || 500).json({ msg: err.message || 'Internal server error' }));
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  return {
    getMine,
    create,
    info,
  };
};

module.exports = CityController;
