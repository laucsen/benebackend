const contries = require('../staticData/countries');
const cities = require('all-the-cities');
const infos = require('get-countries-info').default;

const GeolocationController = () => {
  const countries = async (req, res) => {
    const { filter = '' } = req.query;
    try {
      const result = contries
        .map((c) => (c.name));
      if (filter === '') {
        return res.status(200).json(result);
      }
      const filterLower = filter.toLowerCase();
      return res
        .status(200)
        .json(result.filter((c) => (c.toLowerCase().indexOf(filterLower) >= 0)));
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const city = async (req, res) => {
    const { filter = '' } = req.query;
    try {
      if (filter.length < 4) {
        // Bad Request.
        return res.status(400).json({ msg: 'City filter start with 4 digits' });
      }
      const filterLower = filter.toLowerCase();
      const filteredCities = cities
        .filter((c) => (c.name.toLowerCase().indexOf(filterLower) >= 0))
        .map((c) => (c.name));
      return res.status(200).json(filteredCities);
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const capital = async (req, res) => {
    const { country } = req.params;
    try {
      const cInfo = infos({ name: country }, 'capital');
      return res.status(200).json(cInfo[0]);
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const capitals = async (req, res) => {
    const { filter = '' } = req.query;
    try {
      const caps = contries
        .map((con) => (infos({ name: con.name }, 'capital')[0]))
        .filter((c) => (c !== undefined));
      if (filter === '') {
        return res.status(200).json(caps);
      }
      const filterLower = filter.toLowerCase();
      const filteredCapitals = caps.filter((c) => (c.toLowerCase().indexOf(filterLower) >= 0));
      return res.status(200).json(filteredCapitals);
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  return {
    countries,
    city,
    capital,
    capitals,
  };
};

module.exports = GeolocationController;
