const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');
const infos = require('get-countries-info').default;

const sequelize = require('../../config/database');

const Cities = require('./Cities');

const isValid = (obj) => (obj !== undefined && obj !== null && obj !== '');

const hooks = {
  beforeCreate(user) {
    user.password = bcryptService().password(user); // eslint-disable-line no-param-reassign
  },
  async afterCreate(user) {
    if (isValid(user.country)) {
      const capital = infos({ name: user.country }, 'capital');
      if (isValid(capital) && Array.isArray(capital) && capital.length !== 0) {
        const nc = await Cities.build({
          name: capital[0],
          priority: true,
        }).save();
        nc.setUser(user);
        await nc.save();
      }
    }
  },
};

const tableName = 'users';

const User = sequelize.define('User', {
  email: {
    type: Sequelize.STRING,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
  },
  country: {
    type: Sequelize.STRING,
  },
}, { hooks, tableName });

User.hasMany(Cities, { as: 'cities', foreignKey: 'owner' });
Cities.belongsTo(User, { foreignKey: 'owner' });

// eslint-disable-next-line
User.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());

  delete values.password;

  return values;
};

module.exports = User;
