const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

const tableName = 'cities';

const Cities = sequelize.define('Cities', {
  name: { type: Sequelize.STRING },
  priority: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
}, { tableName });

// eslint-disable-next-line
Cities.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = Cities;
